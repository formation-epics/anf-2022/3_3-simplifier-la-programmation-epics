Présentation de solutions pour simplifier la programmation du framework EPICS (Experimental Physics and Industrial Control System)<p>
The projet is public, you can download the presentation directly from GitLab with Download button.<br/>
From Linux, you can use the commands:
```
wget https://gitlab.com/formation-epics/anf-2022/3_3-simplifier-la-programmation-epics/-/archive/main/3_3-simplifier-la-programmation-epics-main.zip <br/>
unzip 3_3-simplifier-la-programmation-epics-main.zip
```
To modifiy the slides, you need to be a member of the project and clone the project.