# 3.3 Simplifier la programmation EPICS <!-- omit in toc -->


# 1. Informations

## 1.1. Objectifs

- créer un top (projet)
- créer une application
- créer un template
- utiliser le template dans le cmd et dans un fichier de substitutions
- bonnus: créer et utiliser le template dans phoebus

## 1.2. Durée : 60 min

## 1.3. Prérequis

- connaître les commandes Linux ll, cat, mkdir, make, chmod, ~~gedit~~vscode (ou autre éditeur linux).

## 1.4. Déroulement

    ```sh
    # bonjour, bienvenue sur ce tuto macros EPICS, j'espère que ça ne te dérange pas que je te tutoie car ça va durer longtemps !

    # le but de se TP est d'utiliser les macros pour rendre des bases de données EPICS ré-utilisable et donc instanciable plusieurs fois. C'est une notion pratique et très utilisée !

    # on va se baser sur un simple compteur 0 à 10, qu'on va instancier plusieurs fois avec des paramètres en macro.

    # C'et parti !

    # crée le top
    mkdir topSimplifierLaProgrammationEpics
    cd topSimplifierLaProgrammationEpics/

    # crée ton app compteur
    makeBaseApp.pl compteur

    # crée un ioc compteur
    makeBaseApp.pl -i compteur

    # compile ton top: tu n'asncore rien cassé, la compilation doit passer !
    make

    # démare l'ioc
    cd iocBoot/ioccompteur/
    ./st.cmd

    # dans l'iocShell, vérifie que tu as les PVs à l'aide de la commande qui liste les PV de l'ioc
    epics> dbl
    epicslearner:compressExample
    epicslearner:compteur:version
    epicslearner:line:b
    epicslearner:aiExample
    epicslearner:aiExample1
    epicslearner:ai1
    epicslearner:aiExample2
    epicslearner:ai2
    epicslearner:aiExample3
    epicslearner:ai3
    epicslearner:circle:angle
    epicslearner:line:a
    epicslearner:circle:x
    epicslearner:circle:y
    epicslearner:calcExample
    epicslearner:calcExample1
    epicslearner:calc1
    epicslearner:calcExample2
    epicslearner:calc2
    epicslearner:calcExample3
    epicslearner:calc3
    epicslearner:aSubExample
    epicslearner:circle:step
    epicslearner:circle:period
    epicslearner:circle:tick
    epicslearner:subExample
    epicslearner:xxxExample
    epics>

    # bravo, tu as créé un top EPICS qui fonctionne !

    # commente la base de données d'exemple dans st.cmd avec des "#"
    # dbLoadTemplate "db/user.substitutions"
    # dbLoadRecords "db/compteurVersion.db", "user=epicslearner"
    # dbLoadRecords "db/dbSubExample.db", "user=epicslearner"

    # copie le fichier compteur.db dans ton compteurApp
    cd ../.. # top
    cp ../sujet/compteur.db compteurApp/Db/

    # prends le temps d'ouvrir le fichier compteur.db et de comprendre ce qu'il fait:
    # - quel type de record ?
    # - quel est son nom ?
    # - quel est l'utilité de chaque champ EPICS ?
    # - ...

    # ajoute le fichier compteur.db au makefile
    DB += compteur.db

    # liste les fichiers du <TOP>/db/. On voit que compteur.db n'est pas présent
    [epicslearner@rocky topSimplifierLaProgrammationEpics]$ ll db
    total 24
    -rwxrwx---. 1 root vboxsf 1761 Sep 15 09:38 circle.db
    -rwxrwx---. 1 root vboxsf  168 Sep 15 09:38 compteurVersion.db
    -rwxrwx---. 1 root vboxsf 1274 Sep 15 09:38 dbExample1.db
    -rwxrwx---. 1 root vboxsf  921 Sep 15 09:38 dbExample2.db
    -rwxrwx---. 1 root vboxsf  286 Sep 15 09:38 dbSubExample.db
    -rwxrwx---. 1 root vboxsf  327 Sep 15 09:38 user.substitutions

    # compile ton <TOP>
    [epicslearner@rocky topSimplifierLaProgrammationEpics]$ make

    # le make a copié ton <TOP>/compteurApp/Db/compteur.db dans <TOP>/db
    [epicslearner@rocky topSimplifierLaProgrammationEpics]$ ll db
    total 28
    -rwxrwx---. 1 root vboxsf 1761 Sep 15 09:38 circle.db
    -rwxrwx---. 1 root vboxsf  564 Sep 15 10:01 compteur.db
    -rwxrwx---. 1 root vboxsf  168 Sep 15 09:38 compteurVersion.db
    -rwxrwx---. 1 root vboxsf 1274 Sep 15 09:38 dbExample1.db
    -rwxrwx---. 1 root vboxsf  921 Sep 15 09:38 dbExample2.db
    -rwxrwx---. 1 root vboxsf  286 Sep 15 09:38 dbSubExample.db
    -rwxrwx---. 1 root vboxsf  327 Sep 15 09:38 user.substitutions

    # ajoute la base de données de ton compteur à l'ioc.
    ## aide1: ça se passe dans <top>/iocBoot/ioccompteur/st.cmd
    ## aide2: dbLoadTemplate() ou dbLoadRecords() ?

    # puis lance ton ioc et vérifie les PVs présentent avec la commande pour lister les PVs
    epics> <commande qui liste les PVs de l ioc>
    compteur

    # dans un nouveau terminal, fais un camonitor de la PV
    $ camonitor compteur
    compteur                       2022-09-14 15:44:02.687084 0
    compteur                       2022-09-14 15:44:03.687041 1
    compteur                       2022-09-14 15:44:04.695753 2
    compteur                       2022-09-14 15:44:05.690848 3
    compteur                       2022-09-14 15:44:06.687122 4
    compteur                       2022-09-14 15:44:07.687100 5
    compteur                       2022-09-14 15:44:08.687025 6
    compteur                       2022-09-14 15:44:09.687261 7
    compteur                       2022-09-14 15:44:10.687021 8
    compteur                       2022-09-14 15:44:11.687160 9
    compteur                       2022-09-14 15:44:12.687112 0
    compteur                       2022-09-14 15:44:13.687166 1

    # on voit bien la PV varier de 0 à 9 à une période d'une seconde

    # bravo, tu te débrouilles comme un chef.fe !

    # Il est temps de rajouter des macros ! L'objectif est de rajouter des macros sans changer le fonctionnement du compteur. Copie le fichier compteur.db et fais le nécessaire dans le makefile
    $ cp compteurApp/Db/compteur.db compteurApp/Db/compteur_macros.template

    # [INFO] ici on utilise l'extension .template. C'est une convention dans le monde EPICS mais aurait pu laisser .db

    # ajoute des macros pour:
    # - l'index du compteur. Cet index sera utilisé dans le nom de la PV (en suffix)
    # - la plage de comptage avec une macro ${DEBUT} et ${FIN} par exemple
    # - la valeur d'initialisation du compteur
    # - la valeur du scan

    # indice: les macros auront donc des valeurs par défaut.

    # conseil: liste tes macros en entête du fichier comme dans la présentation

    # une fois l'édition du fichier, pose toi la question: est-il nécessaire de compiler ? app ? top ?

    # puis redémare l'ioc

    # vérifie que le compteur fonctionne toujours bien avec le camonitor

    # Maintenant, on va instancier 3 fois le compteur avec 3 plages de comptage différent:
    # - compteur1: 0 à 3
    # - compteur2: 4 à 7
    # - compteur3: 8 à 11

    # pour ce faire, rendez vous dans le st.cmd et rajoute 3 lignes pour tes trois compteurs. Chaque ligne aura les mêmes macros mais avec des valeur différentes !

    # pose toi la question: est-il nécessaire de compiler ? app ? top ?

    # puis redémare l'ioc

    # fais un camonitor avec tes 3 PVs et vérifie le bon fonctionnement
    $ camonitor compteur compteur1 compteur2 compteur3

    # c'est pratique d'instancier dans le cmd, mais c'est plus propre d'utiliser un fichier de substitutions ! Instancie ton compteur 2 autres fois:
    # - compteur4: 12 à 15
    # - compteur5: 16 à 20

    # ajoute ton fichier de subtitution au Makefile et st.cmd

    # pose toi la question: est-il nécessaire de compiler ? app ? top ?

    # fais un camontor avec tes 2 PVs et vérifie le bon fonctionnement
    $ camonitor compteur4 compteur5

    # bravo tu as fini le TP !

    # si tu es en avance, je t'ai prévu un petit bonus afin de réviser ton TP GUI:
    # - crée une vue générique pour afficher ton compteur. Tu peux afficher différents champs du record: sa valeur, sa description et l'opréation qu'il réalise, mais aussi mettre un widget pour changer la valeur du scan
    # - crée une vue qui instancie 6 fois la précédente afin d'afficher tous tes compteurs en même temps !
    ```