TOP=../..

include $(TOP)/configure/CONFIG
#----------------------------------------
#  ADD MACRO DEFINITIONS BELOW HERE

# Use typed rset structure (see 3.16.1 release notes)
USR_CPPFLAGS += -DUSE_TYPED_RSET

# xxxRecord.h will be created from xxxRecord.dbd
DBDINC += xxxRecord

# Install xxxSupport.dbd into <top>/dbd
DBD += xxxSupport.dbd

# Build an IOC support library
LIBRARY_IOC += compteurSupport

# Compile and add code to the support library
compteurSupport_SRCS += xxxRecord.c
compteurSupport_SRCS += devXxxSoft.c

# Link locally-provided code into the support library,
# rather than directly into the IOC application, that
# causes problems on Windows DLL builds
compteurSupport_SRCS += dbSubExample.c
compteurSupport_SRCS += devcompteurVersion.c
compteurSupport_SRCS += compteurHello.c
compteurSupport_SRCS += initTrace.c

compteurSupport_LIBS += $(EPICS_BASE_IOC_LIBS)

# Auto-generate a header file containing a version string.
# Version comes from the VCS if available, else date+time.
GENVERSION = compteurVersion.h
# Macro name
GENVERSIONMACRO = compteurVERSION

# Build the IOC application
PROD_IOC = compteur

# compteur.dbd will be created and installed
DBD += compteur.dbd

# compteur.dbd will include these files:
compteur_DBD += base.dbd
compteur_DBD += xxxSupport.dbd
compteur_DBD += dbSubExample.dbd
compteur_DBD += devcompteurVersion.dbd
compteur_DBD += compteurHello.dbd
compteur_DBD += initTrace.dbd

# compteur_registerRecordDeviceDriver.cpp derives from compteur.dbd
compteur_SRCS += compteur_registerRecordDeviceDriver.cpp

# Build the main IOC entry point where needed
compteur_SRCS_DEFAULT += compteurMain.cpp
compteur_SRCS_vxWorks += -nil-

# Link in the code from our support library
compteur_LIBS += compteurSupport

# To build SNL programs, SNCSEQ must be defined
# in the <top>/configure/RELEASE file
ifneq ($(SNCSEQ),)
    # Build sncExample into compteurSupport
    sncExample_SNCFLAGS += +r
    compteur_DBD += sncExample.dbd
    # A .stt sequence program is *not* pre-processed:
    compteurSupport_SRCS += sncExample.stt
    compteurSupport_LIBS += seq pv
    compteur_LIBS += seq pv

    # Build sncProgram as a standalone program
    PROD_HOST += sncProgram
    sncProgram_SNCFLAGS += +m
    # A .st sequence program *is* pre-processed:
    sncProgram_SRCS += sncProgram.st
    sncProgram_LIBS += seq pv
    sncProgram_LIBS += $(EPICS_BASE_HOST_LIBS)
endif

# Link QSRV (pvAccess Server) if available
ifdef EPICS_QSRV_MAJOR_VERSION
    compteur_LIBS += qsrv
    compteur_LIBS += $(EPICS_BASE_PVA_CORE_LIBS)
    compteur_DBD += PVAServerRegister.dbd
    compteur_DBD += qsrv.dbd
endif

# Finally link IOC to the EPICS Base libraries
compteur_LIBS += $(EPICS_BASE_IOC_LIBS)

include $(TOP)/configure/RULES
#----------------------------------------
#  ADD EXTRA GNUMAKE RULES BELOW HERE

# Explicit dependency needed for generated header file
devcompteurVersion$(DEP): $(COMMON_DIR)/$(GENVERSION)
