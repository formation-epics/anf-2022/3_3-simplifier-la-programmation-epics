#!../../bin/linux-x86_64/compteur

#- You may have to change compteur to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/compteur.dbd"
compteur_registerRecordDeviceDriver pdbbase

## Load record instances
# dbLoadTemplate "db/user.substitutions"
# dbLoadRecords "db/compteurVersion.db", "user=epicslearner"
# dbLoadRecords "db/dbSubExample.db", "user=epicslearner"
dbLoadRecords "db/compteur.db"
dbLoadRecords("db/compteur_macros.template", "IDX=1, DEBUT=0, FIN=3,  VAL=0, SCAN='1 second'")
dbLoadRecords("db/compteur_macros.template", "IDX=2, DEBUT=4, FIN=7,  VAL=4, SCAN='1 second'")
dbLoadRecords("db/compteur_macros.template", "IDX=3, DEBUT=8, FIN=11, VAL=8, SCAN='1 second'")
dbLoadTemplate("db/compteur_macros.substitutions")

#- Set this to see messages from mySub
#var mySubDebug 1

#- Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=epicslearner"
